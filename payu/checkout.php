<?php

$MERCHANT_KEY = "DtCfV5r0";
$SALT = "DXQbyjnFBR";
// $PAYU_BASE_URL = "https://secure.payu.in";
$PAYU_BASE_URL = "https://sandboxsecure.payu.in";
$action = '';

$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
      || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
  $hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';  
  foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }
    $hash_string .= $SALT;
    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<html>
  <head>
  <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  </head>
  <body onload="submitPayuForm()">
    <!-- <h2>PayU Form</h2> -->
    <br/>
    <?php if($formError) { ?>
  
      <span style="color:red">Please fill all mandatory fields.</span>
      <br/>
      <br/>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post" name="payuForm">
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />

      <input name="amount" type="hidden" value="<?php echo (empty($posted['amount'])) ? '1' : $posted['amount'] ?>"/>
      <input name="firstname" type="hidden"  id="firstname" value="<?php echo $row['name']?$row['name']:'Gsingh'; ?>" />
      <input name="email" type="hidden" id="email" value="<?php echo $row['email']; ?>" />
      <input name="phone" type="hidden" value="<?php echo $row['mobile']?$row['mobile']:'7376386005'; ?>" /><textarea name="productinfo" hidden=""><?php echo (empty($posted['productinfo'])) ? 'Exam Fee' : $posted['productinfo'] ?></textarea>
      <input name="surl" type="hidden" value="http://mlsdassociationalld.com/show_detail/" size="64" />
      <input name="furl" type="hidden" value="http://www.mlsdassociationalld.com/wp-content/themes/mlsd_association/response.php" size="64" />
      <input type="hidden" name="service_provider" value="payu_paisa" size="64" /></td>

      <?php if(!$hash) { ?>
      <input type="submit" value="Submit" />
      <?php } ?>

    </form>
